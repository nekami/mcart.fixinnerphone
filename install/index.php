<?
use \Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

Class mcart_fixinnerphone extends CModule
{
	function __construct()
	{
		$arModuleVersion = array();
		include(__DIR__."/version.php");

        $this->MODULE_ID = 'mcart.fixinnerphone';
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("MCART_FIXINNERPHONE_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("MCART_FIXINNERPHONE_MODULE_DESC");
		$this->PARTNER_NAME = Loc::getMessage("MCART_FIXINNERPHONE_PARTNER_NAME");
		$this->PARTNER_URI = Loc::getMessage("MCART_FIXINNERPHONE_PARTNER_URI");
	}

    //Определяем место размещения модуля
    public function GetPath($notDocumentRoot=false)
    {
        if($notDocumentRoot)
            return str_ireplace($_SERVER["DOCUMENT_ROOT"],'',dirname(__DIR__));
        else
            return dirname(__DIR__);
    }

    //Проверяем что система поддерживает D7
    public function isVersionD7()
    {
        return CheckVersion(\Bitrix\Main\ModuleManager::getVersion('main'), '14.00.00');
    }

    function InstallDB()
    {
        return true;
    }

    function UnInstallDB()
    {
        return true;
    }

	function InstallEvents()
	{
        Bitrix\Main\EventManager::getInstance()->registerEventHandlerCompatible('main', 'OnBeforeUserAdd', $this->MODULE_ID, '\Mcart\Fixinnerphone\Event', 'OnBeforeUserAddHandler');		
		
		Bitrix\Main\EventManager::getInstance()->registerEventHandlerCompatible('main', 'OnBeforeUserUpdate', $this->MODULE_ID, '\Mcart\Fixinnerphone\Event', 'OnBeforeUserAddHandler');		
		
		Bitrix\Main\EventManager::getInstance()->registerEventHandlerCompatible('main', 'OnAfterUserAdd', $this->MODULE_ID, '\Mcart\Fixinnerphone\Event', 'OnAfterUserAddHandler');		
		
		Bitrix\Main\EventManager::getInstance()->registerEventHandlerCompatible('main', 'OnAfterUserUpdate', $this->MODULE_ID, '\Mcart\Fixinnerphone\Event', 'OnAfterUserAddHandler');
	}

	function UnInstallEvents()
	{
		Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler('main', 'OnBeforeUserAdd', $this->MODULE_ID, '\Mcart\Fixinnerphone\Event', 'OnBeforeUserAddHandler');

		Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler('main', 'OnBeforeUserUpdate', $this->MODULE_ID, '\Mcart\Fixinnerphone\Event', 'OnBeforeUserAddHandler');

		Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler('main', 'OnAfterUserAdd', $this->MODULE_ID, '\Mcart\Fixinnerphone\Event', 'OnAfterUserAddHandler');	
			
		Bitrix\Main\EventManager::getInstance()->unRegisterEventHandler('main', 'OnAfterUserUpdate', $this->MODULE_ID, '\Mcart\Fixinnerphone\Event', 'OnAfterUserAddHandler');
	}

	function InstallFiles()
	{
        return true;
	}

	function UnInstallFiles()
	{
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
        if($this->isVersionD7())
        {
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();

            \Bitrix\Main\ModuleManager::registerModule($this->MODULE_ID);
        }
        else
        {
            $APPLICATION->ThrowException(Loc::getMessage("MCART_FIXINNERPHONE_INSTALL_ERROR_VERSION"));
        }
	}

	function DoUninstall()
	{
        \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

        $this->UnInstallEvents();
        $this->UnInstallFiles();
        $this->UnInstallDB();
	}
}
?>