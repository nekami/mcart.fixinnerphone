<?
namespace Mcart\Fixinnerphone;

class Event
{	
	public static $SessionPhone;
	
	public static function OnBeforeUserAddHandler(&$arFields)
	{
		if (!empty($arFields["UF_PHONE_INNER"]))
		{
			self::$SessionPhone = preg_replace("/[^0-9]/i", "", $arFields["UF_PHONE_INNER"]);		
			$arFields["UF_PHONE_INNER"] = substr($arFields["UF_PHONE_INNER"], 0, 4); 
			
		}
	}
		
	public static function OnAfterUserAddHandler(&$arFields)
	{
		if (!empty(self::$SessionPhone) && !empty($arFields["UF_PHONE_INNER"])) 
		{
			global $USER_FIELD_MANAGER;
			$resUpdate = $USER_FIELD_MANAGER->Update("USER", $arFields["ID"], array("UF_PHONE_INNER" => self::$SessionPhone));
						
		}
	}	
}