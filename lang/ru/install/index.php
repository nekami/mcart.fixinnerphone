<?
$MESS["MCART_FIXINNERPHONE_MODULE_NAME"] = "Пользовательское поле 'Внутренний телефон'";
$MESS["MCART_FIXINNERPHONE_MODULE_DESC"] = "Убирает ограничения на количество цирф, созданное модулем 'Телефония'";
$MESS["MCART_FIXINNERPHONE_PARTNER_NAME"] = "Эм Си Арт";
$MESS["MCART_FIXINNERPHONE_PARTNER_URI"] = "https://www.mcart.ru/";
$MESS["MCART_FIXINNERPHONE_INSTALL_ERROR_VERSION"] = "Версия главного модуля ниже 14. Не поддерживается технология D7, необходимая модулю. Пожалуйста обновите систему.";
?>